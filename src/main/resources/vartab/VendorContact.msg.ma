" VERSION 2
"******** screens/screen_26/a/Resources.language ********"
      1 de   |Slowakisch
      1 en   |Slovakian
      2 de   |E-Mail
      2 en   |Email
      3 de   |Telefon
      3 en   |Phone
      4 de   |Griechisch
      4 en   |Greek
      5 de   |Individuelle Sprache 1
      5 en   |1st individual language
      6 de   |Website
      6 en   |Website
      7 de   |Text 1
      7 en   |Text 1
      8 de   |Anrede
      8 en   |Salutation
      9 de   |Mazedonisch
      9 en   |Macedonian
     10 de   |Tschechisch
     10 en   |Czech
     11 de   |Bulgarisch
     11 en   |Bulgarian
     12 de   |Italienisch
     12 en   |Italian
     13 de   |Suchwort
     13 en   |Search word
     14 de   |Website besuchen
     14 en   |Visit website
     15 de   |Übersetzen
     15 en   |Translate
     16 de   |Englisch
     16 en   |English
     17 de   |Individuelle Sprache 3
     17 en   |3rd individual language
     18 de   |Slowenisch
     18 en   |Slovenian
     19 de   |Konsignationslagerplatz
     19 en   |Consignment warehouse location
     20 de   |Lieferbedingung
     20 en   |Delivery terms
     21 de   |Betreuer
     21 en   |Inhouse contact
     22 de   |Serbisch
     22 en   |Serbian
     23 de   |Ort
     23 en   |Town
     24 de   |Text 2
     24 en   |Text 2
     25 de   |EG-Kennung
     25 en   |
     26 de   |Name
     26 en   |Name
     27 de   |Landart
     27 en   |Country type
     28 de   |Ungarisch
     28 en   |Hungarian
     29 de   |Allgemeines
     29 en   |General
     30 de   |Land
     30 en   |Country
     31 de   |Russisch
     31 en   |Russian
     32 de   |Bezeichnung
     32 en   |Description
     33 de   |DUNS
     33 en   |DUNS
     34 de   |Identnummer
     34 en   |Identity number
     35 de   |Anschrift Firma
     35 en   |Company address
     36 de   |Deutsch
     36 en   |German
     37 de   |Fax
     37 en   |Fax
     38 de   |Portugiesisch
     38 en   |Portuguese
     39 de   |Mahnsperre
     39 en   |Reminder lock
     40 de   |Abteilung
     40 en   |Department
     41 de   |Steuerbefreiungsnummer
     41 en   |Tax exemption number
     42 de   |Rumänisch
     42 en   |Romanian
     43 de   |Indonesisch
     43 en   |Indonesian
     44 de   |Französisch
     44 en   |French
     45 de   |Arabisch
     45 en   |Arabic
     46 de   |Chinesisch vereinfacht
     46 en   |Chinese,simplified
     47 de   |Mobiltelefon
     47 en   |Mobile phone
     48 de   |E-Mail senden
     48 en   |Send email
     49 de   |Individuelle Sprache 2
     49 en   |2nd individual language
     50 de   |Thailändisch
     50 en   |Thai
     51 de   |Abladestelle
     51 en   |Unloading point
     52 de   |Lieferant
     52 en   |Supplier
     53 de   |Lieferantenkontakt
     53 en   |Supplier contact
     54 de   |Editieren
     54 en   |Edit
     55 de   |Straße
     55 en   |Street
     56 de   |Polnisch
     56 en   |Polish
     57 de   |Ansprechpartner
     57 en   |Contact person
     58 de   |Persisch
     58 en   |Persian
     59 de   |Kennzeichen
     59 en   |Code
     60 de   |Versand, Warenverkehr
     60 en   |Shipping, Goods traffic
     61 de   |Spanisch
     61 en   |Spanish
     62 de   |Vietnamesisch
     62 en   |Vietnamese
     63 de   |Umsatzsteuer-Identifikationsnummer
     63 en   |VAT registration number
     64 de   |Bemerkungen
     64 en   |Remarks
     65 de   |Niederländisch
     65 en   |Dutch
     66 de   |Rechnung
     66 en   |Invoice
     67 de   |Region
     67 en   |Region
     68 de   |Türkisch
     68 en   |Turkish
     69 de   |Chinesisch traditionell
     69 en   |Chinese,traditional
     70 de   |Urdu
     70 en   |Urdu
     71 de   |Amerikanisches Englisch
     71 en   |American English
     72 de   |Sprache
     72 en   |Language
     73 de   |EDI
     73 en   |EDI
     74 de   |EDI-Nachrichten
     74 en   |EDI messages
     75 de   |Schlüssel Versandart
     75 en   |Shipping type code
     76 de   |GLN
     76 en   |GLN
     77 de   |Werk
     77 en   |Plant
     78 de   |Versand
     78 en   |Shipping
     79 de   |Merkmale
     79 en   |Characteristics
     80 de   |Seite 1
     80 en   |Page 1
     81 de   |Koordinaten
     81 en   |Coordinates
     82 de   |Anschrift privat
     82 en   |Home address
     83 de   |Längengrad
     83 en   |Longitude
     84 de   |Breitengrad
     84 en   |Latitude
     85 de   |Postleitzahl
     85 en   |Postcode
     86 de   |Zollkennzeichen
     86 en   |Customs tariff number
     87 de   |Art des Geschäfts
     87 en   |Nature of business
     88 de   |ATLAS
     88 en   |ATLAS
     89 de   |Teilnehmer-Identifikationsnummer
     89 en   |Participant identification number
     90 de   |Angemeldetes Verfahren
     90 en   |Registered procedure
     91 de   |Vorangegangenes Verfahren
     91 en   |Previous procedure
     92 de   |Weiteres Verfahren
     92 en   |Further procedure
     93 de   |Beförderungsmittel im Inland
     93 en   |Inland means of transport
     94 de   |Verkehrszweig
     94 en   |Mode of transport
     95 de   |Beförderungsmittel an der Grenze
     95 en   |Means of transport at border
     96 de   |Art
     96 en   |Type
     97 de   |Japanisch
     97 en   |Japanese
     98 de   |Hidden fields which must remain in the screen because of loaders
     98 en   |
     99 de   |Intrastat
     99 en   |Intrastat
    100 de   |DMS
    100 en   |DMS
    101 de   |Letztes Dokument
    101 en   |Last document
    102 de   |Öffnen
    102 en   |Open
    103 de   |Dokumente archivieren
    103 en   |Archive documents
    104 de   |Belegart
    104 en   |Document type
    105 de   |Barcode
    105 en   |Barcode
    106 de   |Zuordnen
    106 en   |Allocate
    107 de   |Betreff
    107 en   |Subject
    108 de   |Datei
    108 en   |File
    109 de   |Selektion für Datenexport
    109 en   |
