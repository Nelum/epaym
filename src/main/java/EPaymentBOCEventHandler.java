package com.nelumdev.abas.epay;

import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.abas.jfop.base.Color;
import de.abas.jfop.base.buffer.BufferFactory;
import de.abas.jfop.base.buffer.GlobalTextBuffer;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.annotation.EventHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.function.Supplier;

//import com.sun.xml.internal.ws.util.QNameMap.Entry;

import de.abas.eks.jfop.remote.FOe;
import de.abas.erp.api.gui.ButtonSet;
import de.abas.erp.api.gui.TextBox;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.db.schema.account.Account;
import de.abas.erp.db.schema.account.SelectableAccount;
import de.abas.erp.db.schema.custom.bulkepaymentboc.EPaymentBOC;
import de.abas.erp.db.schema.custom.bulkepaymentboc.EPaymentBOCEditor;
import de.abas.erp.db.schema.custom.bulkepaymentboc.EPaymentBOCEditor.Row;
import de.abas.erp.db.schema.numberassignment.NumberRange;
import de.abas.erp.db.schema.custom.bulkepaymentboc.EPaymentSettings;
import de.abas.erp.db.schema.entry.EntryEditor;
import de.abas.erp.db.schema.entry.SelectableEntry;
import de.abas.erp.db.schema.purchasing.Invoice;
import de.abas.erp.db.schema.purchasing.Purchasing;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.ExpertSelection;
import de.abas.erp.db.selection.RowSelectionBuilder;
import de.abas.erp.db.selection.Selection;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.db.util.QueryUtil;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.common.type.AbasDate;
import de.abas.erp.common.type.IdImpl;
import de.abas.erp.common.type.enums.EnumDialogBox;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.FieldValueProvider;
import de.abas.erp.db.Query;
import de.abas.erp.db.RowQuery;
import de.abas.erp.db.RowRecord;
import de.abas.erp.db.SelectableObject;
import de.abas.erp.db.exception.CommandException;
import de.abas.erp.axi2.type.ButtonEventType;

@EventHandler(head = EPaymentBOCEditor.class)

@RunFopWith(EventHandlerRunner.class)

public class EPaymentBOCEventHandler {

	@ButtonEventHandler(field="yepaymload", type = ButtonEventType.AFTER)
	public void yepaymloadAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, EPaymentBOCEditor head) throws EventException{
		ctx.out().println("Test");
		head.table().clear();
        HashMap<String, String> eInvoice = getEpaymentInvoiceList(ctx);
		Query<Invoice> invoiceQuery      = getPurchaseInvoiceList(ctx);
		addInvoice(ctx, head, eInvoice, invoiceQuery);
	}


	private Query<Invoice> getPurchaseInvoiceList(DbContext ctx) {
		String invoiceCriteria = "@language=de;such=;sucherw~/;@order=lief;sumnetto>0;@group=24;@filingmode=(Filed)";
		Selection<Invoice> selection = ExpertSelection.create(Invoice.class, invoiceCriteria);  
		Query<Invoice> invoiceQuery  = ctx.createQuery(selection);
		return invoiceQuery;
	}




	private void addInvoice(DbContext ctx, EPaymentBOCEditor head, HashMap<String, String> eInvoice,
			Query<Invoice> invoiceQuery) {
		for(Invoice invoice : invoiceQuery ){
			String invoiceNumber = invoice.getIdno().toString();
			if(!eInvoice.containsKey(invoiceNumber)){
		    	Row appendRow = head.table().appendRow();
				setRowValues(ctx, invoice, appendRow);
		    }
		}
	}




	private void setRowValues(DbContext ctx, Invoice invoice, Row appendRow) {
		appendRow.setYepayminvid(invoice.getIdno());
		appendRow.setYepaymsupp (getSupplier(invoice.getVendor().getIdno(),ctx));
		appendRow.setYepaymsuppname(invoice.getString("vendor^descrOperLang"));
		appendRow.setYepayminvamt(invoice.getTotalNetAmt());
		appendRow.setYepaympayamt(invoice.getTotalNetAmt());
		appendRow.setYepaymstatus("icon:lock");
		appendRow.setYepaympaydes(invoice.getComments());
		appendRow.setYepaymacc(invoice.getString("vendor^bankAcctHolderName"));
		appendRow.setYepaymaccno(invoice.getString("vendor^bankAcctNo"));
		appendRow.setYepaymmobile(invoice.getString("vendor^yepaymmobile"));
		appendRow.setYepaymemail(invoice.getString("vendor^yepaymemail"));
		appendRow.setYepaymduedate(invoice.getDeadlineWeek());
		appendRow.setYepaymtrdate(invoice.getDateFrom());
	}




	private HashMap<String, String> getEpaymentInvoiceList(DbContext ctx) {
		String crt = "@language=de;0:nummer=;@group=1;@filingmode=(Active);@rows=(Yes) ";
		Selection<EPaymentBOC.Row> epaymentrows = ExpertSelection.create(EPaymentBOC.Row.class,crt);
		Query<EPaymentBOC.Row> query = ctx.createQuery(epaymentrows);
		
		HashMap<String,String> eInvoice = new HashMap<String,String>();
		for(EPaymentBOC.Row row : query){
			eInvoice.put(row.getYepayminvid(), row.getYepayminvid());
		}
		return eInvoice;
	}



	
	@ScreenEventHandler(type = ScreenEventType.ENTER)
	public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, EPaymentBOCEditor head) throws EventException{
		if(ScreenMode.isNew(event)){	
			head.table().clear();					
			head.setYepaympaydate(new AbasDate());
			loadSettings(ctx, head);
		}	
		
	}



	public void loadSettings(DbContext ctx, EPaymentBOCEditor head) {
		String  settingCriteria = "@language=de;nummer=;such==EPAYMENT;sucherw~/;@group=0;@filingmode=(Active) ";
		Selection<EPaymentSettings> settingObject = ExpertSelection.create(EPaymentSettings.class, settingCriteria);
		if(settingObject!= null){
			head.setYepaymsett(settingObject);
		}else{
		   new TextBox(ctx,"Create Settings","Create Settings before started").show();
		}
	}
	
	
	
	
	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, EPaymentBOCEditor head) throws EventException{
		validateSupplierDetails(screenControl, head);
		if(ScreenMode.isNew(event)){
			generateSearchWord(head);
		}
	}




	private void generateSearchWord(EPaymentBOCEditor head) {
		HashMap<String,String> sdata = new HashMap<String,String> ();
		sdata = getGlobalData();
		String time = sdata.get("STIME2");
		time = time.replaceAll(":", "");
		head.setSwd("EPAY"+ sdata.get("SDATE2") + time);
		try {
			head.commitAndReopen();
		} catch (CommandException e) {
			e.printStackTrace();
		}
	}




	private void validateSupplierDetails(ScreenControl screenControl, EPaymentBOCEditor head) throws EventException {
		Iterable<EPaymentBOC.Row> validrow = head.table().getRows();
		for(EPaymentBOC.Row row : validrow ){
				if(row.getYepaymacc().isEmpty()|| row.getYepaymaccno().isEmpty() ){
				screenControl.moveCursor(row, "yepaymacc");
				throw new EventException ("Please fill Supplier account details");
			}
		}
	}

	public void validateAccountDetails(ScreenControl screenControl, EPaymentBOC.Row row) throws EventException {
		if(row.getYepaymacc().isEmpty()|| row.getYepaymaccno().isEmpty() ){
			screenControl.moveCursor(row, "yepaymacc");
			throw new EventException ("Please fill Supplier account details");
		}
	}
	
	@ScreenEventHandler(type = ScreenEventType.EXIT)
	public void screenExit(ScreenEvent event, ScreenControl screenControl, DbContext ctx, EPaymentBOCEditor head) throws EventException, IOException{
		if(ScreenMode.isEdit(event)){
			if(head.getYepaymapp()&& !head.getYepaymfiledone()){
				FileHandler.createFile(ctx, head);				
			}
		}
		
	}
	
	
	public Vendor getSupplier(String supid,DbContext ctx){
		SelectionBuilder<Vendor> selven = SelectionBuilder.create(Vendor.class);
		selven.add(Conditions.eq(Vendor.META.idno, supid));
		Vendor ven = QueryUtil.getFirst(ctx, selven.build());
		return ven;
	}
	
	public Account getAccount(String accountid,DbContext ctx){
		SelectionBuilder<Account> account = SelectionBuilder.create(Account.class);
		account.add(Conditions.eq(Account.META.idno, accountid));
		Account accountNumber = QueryUtil.getFirst(ctx, account.build());
		return accountNumber;
	}
	
	
	public EPaymentBOC getEpayid(String eayid,DbContext ctx){
		SelectionBuilder<EPaymentBOC> epay = SelectionBuilder.create(EPaymentBOC.class);
		epay.add(Conditions.eq(EPaymentBOC.META.idno, eayid));
		EPaymentBOC payid = QueryUtil.getFirst(ctx, epay.build());
		return payid;
	}
	
	public HashMap<String,String> getGlobalData(){
		BufferFactory bufferFactory = BufferFactory.newInstance(true);
		GlobalTextBuffer globalText = bufferFactory.getGlobalTextBuffer();
		HashMap<String,String> gdata = new HashMap<String,String>();
		gdata.put("OPERATOR", globalText.getStringValue("operatorCode"));
		gdata.put("SDATE", globalText.getStringValue("date"));
		gdata.put("SDATE2", globalText.getAbasDateValue("date").toString());
		gdata.put("STIME", globalText.getStringValue("currTime"));
		gdata.put("STIME2", globalText.getStringValue("currTime").toString());
		return gdata;
	}
	

	@FieldEventHandler(field="yepaymapp", type = FieldEventType.EXIT)
	public void yepaymappExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, EPaymentBOCEditor head) throws EventException{
		if(head.getYepaymapp()!=true){
			head.setYepaymappuser("");
		}else{
			setApproverDetails(head);
		}
	}


	private void setApproverDetails(EPaymentBOCEditor head) {
		BufferFactory bufferFactory = BufferFactory.newInstance(true);
		GlobalTextBuffer globalText = bufferFactory.getGlobalTextBuffer();
		head.setYepaymappuser( globalText.getStringValue("operatorCode")
						+ globalText.getAbasDateValue("date")+
						globalText.getStringValue("currTime") );
	}




	
	@ButtonEventHandler(field="yepaymupload", type = ButtonEventType.AFTER)
	public void yepaymuploadAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, EPaymentBOCEditor head) throws EventException{
		String user     = head.getString("yepaymsett^yepaymuser");
		String password = head.getString("yepaymsett^yepaympw");
		String sever    = head.getString("yepaymsett^yepaymhost");
		Integer port    = head.getInteger("yepaymsett^yepaymport");		
		FtpFileUpload ftpFile = new FtpFileUpload();
		if(ftpFile.uploadFile(sever,port,user,password,head.getYepaymfilename())){
			new TextBox(ctx,"Upload",head.getYepaymfilename() +" Uploaded Successfully").show();
		}else{
			new TextBox(ctx,"Upload Fail","File upload failed").show();
		}	
		File file = new File(head.getYepaymfilename());
		file.delete();
	}

	
	@ButtonEventHandler(field="yepaymevalsucc", type = ButtonEventType.AFTER)
	public void yepaymevalsuccAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, EPaymentBOCEditor head) throws EventException{
		if(ScreenMode.isView(event)){
			TextBox entryMessage = new TextBox(ctx,"Generate Entry","Generate entry and file ePayment");
			entryMessage.setButtons(ButtonSet.YES_NO);
			EnumDialogBox result = entryMessage.show();
			if(result.equals(EnumDialogBox.Yes)){
					createEntry(ctx, head);		
					new TextBox(ctx,"Entry Created", "Entry Successfully created").show();
			}
			
		}
		
	}


	private void createEntry(DbContext ctx, EPaymentBOCEditor head) {
		Iterable<EPaymentBOCEditor.Row> iterableRows = head.table().getEditableRows();
		for(EPaymentBOCEditor.Row row:iterableRows){
			EntryEditor newEntry = ctx.newObject(EntryEditor.class);
			newEntry.setDocNo(row.getYepayminvid());
			newEntry.setDateDoc(new AbasDate());
			newEntry.setDescrText(row.getYepaympaydes()+ row.getYepayminvid());
			EntryEditor.Row newRow = newEntry.table().appendRow();
			newRow.setEntAcct(getSupplier(row.getYepaymsupp().getIdno(),ctx));
			newRow.setRecAmt(row.getYepaympayamt());
			EntryEditor.Row secondRow = newEntry.table().appendRow();
			//secondRow.setEntAcct(head.getYepaymsett().getYepaymmacc());
			secondRow.setEntCurrCred(row.getYepaympayamt());
			try {
				newEntry.commitAndReopen();
				String entryno = newEntry.getIdno();
				newEntry.abort();
				updateEntry(ctx, row, row.getYepayminvid(),entryno);		
			} catch (CommandException e) {
				e.printStackTrace();
				ctx.out().println(e.getMessage());
			}
		}
		
	}
	
	
	private void updateEntry(DbContext ctx,EPaymentBOC.Row epaymentRow, String invoice, String entryno){
		EPaymentBOC epaymentBoc = epaymentRow.header();
		EPaymentBOCEditor epaymentBocEditor = epaymentBoc.createEditor();
		try {
			epaymentBocEditor.open(EditorAction.UPDATE);
			Iterable<EPaymentBOCEditor.Row> rows = epaymentBocEditor.table().getEditableRows();
			for(EPaymentBOCEditor.Row row: rows){
				if(row.getYepayminvid().trim().equals(invoice)){
					row.setYepaymentry(entryno);
					break;
				}
				row.header().setYepaymconsol(true);
			}
			epaymentBocEditor.commit();
		} catch (CommandException e) {
			e.printStackTrace();
		}
		
	}
	

		
	
}


